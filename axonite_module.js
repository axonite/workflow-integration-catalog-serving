'use strict';
const packageJson = require('./package.json');
const Service = require('./api').describeService();
const CatalogSync = require('@axonite.io/axonite-catalog-sync');

class AxoniteModule {
  constructor(){
    this.name = packageJson.name;
    this.version = packageJson.version;

    this.Topics = {
      workflowEvents: 'workflowEvents'
    };
    this.Topics[Service.naturalKey] = Service.naturalKey;
    this.Topics[CatalogSync.Topics.catalogSyncEvents] = CatalogSync.Topics.catalogSyncEvents;

    this.Queues = {
      workflowEvents: 'workflowEvents'
    };
    this.Queues[Service.naturalKey] = Service.naturalKey;
  }
  
  getCloudFormation(){
    var workflowCommands = Service.naturalKey;
    return [
      {
        tag: 'commands',
        queue: workflowCommands,
        subscribedTopics: [
          workflowCommands,
          CatalogSync.Topics.catalogSyncEvents
        ]
      },
      {
        tag: 'events',
        queue: this.Queues.workflowEvents,
        subscribedTopics: [this.Topics.workflowEvents]
      }
    ];
  }

  getQueueProcessors() {
    return require('./lib/module/queue_processors').getQueueProcessors();
  }
}

module.exports = new AxoniteModule();