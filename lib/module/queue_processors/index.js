'use strict';
var CommandHandler = require('@axonite.io/axonite-core/lib/command_handler');
var QueueProcessor = require('@axonite.io/axonite-core/lib/queue_processor');
var Service = require('../../../api').describeService();
var Aggregate = require('../aggregate/workflow_aggregate');

module.exports = {
  getQueueProcessors: function(){

    var queueProcessors = [];

    var commandHandler = new CommandHandler({Aggregate: Aggregate});
    var queueProcessor = new QueueProcessor({
      queueName: Service.naturalKey,
      messageHandler: commandHandler.handle.bind(commandHandler)
    });
    queueProcessors.push(queueProcessor);

    return queueProcessors;
  }
};