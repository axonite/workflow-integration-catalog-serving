'use strict';
var CatalogChangefeedWorkflow  = require('@axonite.io/axonite-workflow/lib/module/aggregate/catalog_changefeed_workflow');
var logger = require('@axonite.io/axonite-core/lib/logger');
const Service = require('../../../api').describeService();
const CatalogServingModule = require('@axonite.io/axonite-catalog-serving');

class ImportBusinessUnitsWorkflow extends CatalogChangefeedWorkflow {
  constructor(opt) {
    var catalogChangefeedWorkflow = {
      service:Service,
      stages: {
        executeWorkflow: {
          intermediateEvent: Module.Events.executingWorkflow,
          finalEvent: Module.Events.workflowExecuted,
          steps: [{
            name: 'executeWorkflowSummary'
          }]
        }
      }
    };
    super(opt, catalogChangefeedWorkflow);
  }
  
  executeWorkflowSummary(workflow) {
    return CatalogServingModule.executeSubscription({subscription: workflow.subscription})
      .then(summary => {
        logger.info(workflow.id, '[', workflow.service.naturalKey, '] executeWorkflowSummary', JSON.stringify(summary));
        workflow.cycle.lastExecution = summary;
        this.setNextExecution(workflow);
      });
  }



}

module.exports = ImportBusinessUnitsWorkflow;